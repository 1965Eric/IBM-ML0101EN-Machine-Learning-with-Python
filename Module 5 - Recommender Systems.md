## Module Introduction

In this module, you will learn about recommender systems. You will be introduced to the main idea behind recommendation engines, then you will gain an understanding of two main types of recommendation engines, namely, content-based and collaborative filtering.

## Learning Objectives

* To understand the purpose and mechanism of recommendation systems.
* To understand the different types of recommender systems. 
* To implement recommender systems on a real data set.

## Content-based Recommendation Systems

[Content-based Recommendation Systems](https://github.com/1965Eric/IBM-ML0101EN-Machine-Learning-with-Python/blob/main/ML0101EN-RecSys-Content-Based-movies.ipynb)

